import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatApplicationComponent } from './chat-application/chat-application.component';
import { ChatHistoryComponent } from './chat-history/chat-history.component';
import { FaqBySubjectComponent } from './faq-by-subject/faq-by-subject.component';
//import { ChatHistoryComponent } from './chat-history/chat-history.component';
import { FaqComponent } from './faq/faq.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LogInPageComponent } from './log-in-page/log-in-page.component';
import { MentorProfileComponent } from './mentor-profile/mentor-profile.component';
import { MentorRegistrationComponent } from './mentor-registration/mentor-registration.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterationPageComponent } from './registeration-page/registeration-page.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { StudentliveconnectComponent } from './studentliveconnect/studentliveconnect.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';

const routes: Routes = [
  { path: "student-profile", component: StudentProfileComponent },
  { path: "login", component: LogInPageComponent },
  { path: "student-registration", component: RegisterationPageComponent },
  { path: "home", component: HomepageComponent },
  // { path: "liveconnect", component: StudentliveconnectComponent },
  { path: "mentor-profile", component: MentorProfileComponent },
  { path: 'feedback/:sid', component: FeedbackComponent },
  { path: "update-profile", component: UpdateProfileComponent },
  { path: "frequently-asked-questions", component: FaqComponent },
  { path: "chat/:id", component: ChatApplicationComponent },
  { path: "mentor-registration", component: MentorRegistrationComponent },
  {path: "faq/:subject", component: FaqBySubjectComponent},
  { path: "history/:sessionId", component: ChatHistoryComponent },
  { path: '', component: LandingPageComponent, pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
